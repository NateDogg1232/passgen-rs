# passxgen

A password generator library/CLI application written in Rust for Rust

## CLI Usage
General usage:
```
USAGE:
    passxgen <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    help        Prints this message or the help of the given subcommand(s)
    password    Generate a password
```
For password generation:
```
USAGE:
    passxgen password [FLAGS] <length>

FLAGS:
    -b               Begin with a letter
    -h, --help       Prints help information
    -l               Lowercase letters
    -N               No similar characters (eg. 0 and o and O)
    -n               Numbers
    -s               Symbols
    -u               Uppercase letters
    -V, --version    Prints version information

ARGS:
    <length>  
```
For passphrase generation
```
USAGE:
    passxgen passphrase [FLAGS] [OPTIONS] <words>

FLAGS:
    -c, --capitalize    Capitalize each first letter of the word
    -h, --help          Prints help information
    -V, --version       Prints version information

OPTIONS:
    -d, --dictionary <dictionary>    Use an external file as a dictionary. Default is the short eff list
    -s, --separator <separator>      Separator between each word [default: .]

ARGS:
    <words>    Number of words to have in phrase
```

## Library usage
Take a look over the documentation by cloning and running the command `cargo doc --lib --open` Once I feel that the documentation coverage is acceptable, I will post this to crates.io for usage.

## Compiling
```sh
git clone https://gitlab.com/NateDogg1232/passgen-rs.git
cd passgen-rs
cargo build
```

## Contributing
### Find an issue
- Find an issue in the issue tracker.
- State that you want to work on it by commenting on it
- Make a pull request once it's done

I will try to be timely in reviewing pull requests and issues.
