#![warn(missing_docs)]
use passxgen::password::PasswordGenerator;
use passxgen::passphrase::{PassphraseGenerator, DEFAULT_DICT};
use std::io::Read;
use std::path::PathBuf;
use structopt::clap::ArgGroup;
use structopt::StructOpt;

fn letters_group() -> ArgGroup<'static> {
    ArgGroup::with_name("letters")
        .required(true)
        .multiple(true)
        //Sadly, in order to get structopt to cooperate with us, we must hardcode these
        .args(&["lowercase", "uppercase"])
}

#[derive(Debug, StructOpt)]
enum Subcommands {
    #[structopt(
        name = "passphrase",
        about = "Generate a passphrase",
        version = "0.1.0"
    )]
    Phrase(PhraseOpts),
    #[structopt(name = "password", about = "Generate a password", version = "0.2.0")]
    Word(WordOpts),
    // #[structopt(name="pattern")]
    // Pattern {
    //
    // }
}

#[derive(Debug, StructOpt)]
pub struct PhraseOpts {
    #[structopt(help = "Number of words to have in phrase")]
    words: usize,
    #[structopt(
        long,
        short,
        help = "Use an external file as a dictionary. Default is the short eff list",
        parse(from_os_str)
    )]
    dictionary: Option<PathBuf>,
    #[structopt(long, short, help = "Separator between each word", default_value = ".")]
    separator: String,
    #[structopt(
        short = "c",
        long = "capitalize",
        help = "Capitalize each first letter of the word"
    )]
    capitalize: bool,
}

#[derive(Debug, StructOpt)]
pub struct WordOpts {
    #[structopt(required = true)]
    length: usize,
    #[structopt(flatten)]
    letters: Letters,
    #[structopt(short = "b", help = "Begin with a letter")]
    begin_with_letter: bool,
    #[structopt(short = "s", help = "Symbols")]
    symbols: bool,
    #[structopt(short = "n", help = "Numbers")]
    numbers: bool,
    #[structopt(short = "N", help = "No similar characters (eg. 0 and o and O)")]
    no_similar_chars: bool,
}

#[derive(Debug, StructOpt)]
#[structopt(raw(group = "letters_group()"))]
pub struct Letters {
    #[structopt(short = "l", help = "Lowercase letters")]
    lowercase: bool,
    #[structopt(short = "u", help = "Uppercase letters")]
    uppercase: bool,
}

fn main() {
    let args = Subcommands::from_args();
    #[allow(unreachable_patterns)]
    match args {
        Subcommands::Word(opts) => {
            //We make a password generator based on our options
            let passgen = PasswordGenerator::new(opts.length)
                .symbols(opts.symbols)
                .numbers(opts.numbers)
                .uppercase(opts.letters.uppercase)
                .lowercase(opts.letters.lowercase)
                .begin_with_letter(opts.begin_with_letter)
                .no_similar_chars(opts.no_similar_chars);
            println!("{}", passgen.generate().unwrap());
        }
        Subcommands::Phrase(opts) => {
            //Let's get our dictionary
            let dict = match opts.dictionary {
                Some(s) => {
                    //Open the dictionary
                    let dict: String = match std::fs::File::open(s) {
                        Ok(mut f) => {
                            let mut tmp_str = String::new();
                            f.read_to_string(&mut tmp_str).unwrap();
                            tmp_str
                        },
                        Err(e) => {
                            eprintln!("Could not open dictionary file: {}", e);
                            eprintln!("Using default dictionary");
                            DEFAULT_DICT.to_owned()
                        }
                    };
                    dict
                },
                None => String::from(DEFAULT_DICT),
            };
            let phrasegen = PassphraseGenerator::new()
                .dict(dict).unwrap()
                .words(opts.words).unwrap()
                .separator(opts.separator)
                .capitalize(opts.capitalize);
            println!("{}", phrasegen.generate().unwrap());

        }
        _ => unimplemented!("No other subcommands implemented yet"),
    }
}
