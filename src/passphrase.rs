#![warn(missing_docs)]
use crate::symbolgen;

/// The default dictionary used for the generator. This is the [eff short list](https://www.eff.org/files/2016/09/08/eff_short_wordlist_2_0.txt)
pub static DEFAULT_DICT: &str = include_str!("../dicts/dict.txt");

#[derive(Debug)]
pub struct PassphraseGenerator {
    words: usize,
    dict: String,
    dict_set: bool,
    separator: String,
    separator_set: bool,
    capitalize: bool,
}

#[derive(Debug)]
pub enum PassphraseError {
    EmptyDict,
    WordLengthLessThanOne,
    SeparatorNotSet,
}

impl PassphraseGenerator {
    pub fn new() -> PassphraseGenerator {
        PassphraseGenerator {
            words: 0,
            dict: String::from(""),
            dict_set: false,
            separator: String::from(""),
            separator_set: false,
            capitalize: false,
        }
    }
    pub fn dict(mut self, dict: String) -> Result<Self, PassphraseError> {
        if dict.is_empty() {
            Err(PassphraseError::EmptyDict)
        } else {
            self.dict = dict;
            self.dict_set = true;
            Ok(self)
        }
    }
    pub fn words(mut self, words: usize) -> Result<Self, PassphraseError> {
        if words < 1 {
            Err(PassphraseError::WordLengthLessThanOne)
        } else {
            self.words = words;
            Ok(self)
        }
    }
    pub fn separator(mut self, sep: String) -> Self {
        self.separator = sep;
        self.separator_set = true;
        self
    }
    pub fn capitalize(mut self, capitalize: bool) -> Self {
        self.capitalize = capitalize;
        self
    }

    pub fn generate(&self) -> Result<String, PassphraseError> {
        self.check()?;
        let mut ret_string = String::new();
        for _ in 0..self.words - 1 {
            ret_string.push_str(self.gen_word().as_str());
            ret_string.push_str(&self.separator);
        }
        //Push the final word without the separator
        ret_string.push_str(self.gen_word().as_str());
        Ok(ret_string)
    }

    fn gen_word(&self) -> String {
        let mut word = symbolgen::gen_symbol(&self.dict);
        if self.capitalize {
            let mut tmp: Vec<char> = word.chars().collect();
            tmp[0] = tmp[0].to_uppercase().nth(0).unwrap();
            word = tmp.iter().collect();
        }
        word
    }

    pub fn check(&self) -> Result<(), PassphraseError> {
        if !self.dict_set {
            Err(PassphraseError::EmptyDict)
        } else if self.dict.is_empty() {
            Err(PassphraseError::EmptyDict)
        } else if !self.separator_set {
            Err(PassphraseError::SeparatorNotSet)
        } else if  self.words < 1 {
            Err(PassphraseError::WordLengthLessThanOne)
        } else {
            Ok(())
        }
    }
}

impl Default for PassphraseGenerator {
    fn default() -> PassphraseGenerator {
        PassphraseGenerator {
            words: 5,
            dict: String::from(DEFAULT_DICT),
            dict_set: true,
            separator: String::from("."),
            separator_set: true,
            capitalize: true,
        }
    }
}

// use crate::PhraseOpts;
// use rand::prelude::*;
// use std::io::prelude::*;
// use std::path::PathBuf;
//
// static DEFAULT_DICT: &str = include_str!("../dicts/dict.txt");
//
// pub fn gen_passphrase(opts: PhraseOpts) -> String {
//     //Open up the dictionary file, if any
//     #[allow(unused_variables)]
//     let dict = match opts.dictionary {
//         Some(s) => {
//             //Open the dictionary
//             let dict: String = match std::fs::File::open(s) {
//                 Ok(mut f) => {
//                     let mut tmp_str = String::new();
//                     f.read_to_string(&mut tmp_str).unwrap();
//                     tmp_str
//                 },
//                 Err(e) => {
//                     eprintln!("Could not open dictionary file: {}", e);
//                     eprintln!("Using default dictionary");
//                     DEFAULT_DICT.to_owned()
//                 }
//             };
//             dict
//         },
//         None => String::from(DEFAULT_DICT),
//     };
//     let mut pass_string = String::new();
//     for _ in 0..opts.words {
//         let mut word = gen_word(&dict);
//         if opts.capitalize {
//             let mut tmp: Vec<char> = word.chars().collect();
//             tmp[0] = tmp[0].to_uppercase().nth(0).unwrap();
//             word = tmp.iter().collect();
//         }
//         pass_string.push_str(word.as_str());
//         pass_string.push_str(opts.separator.as_str());
//     }
//     pass_string
// }
//
// fn gen_word(dictionary: &String) -> String {
//     //Let's split up the dictionary by whitespace
//     let dict: Vec<&str> = dictionary.split_whitespace().collect();
//     //And we get a random word out of it
//     let rand = thread_rng().gen_range(0, dict.len());
//     dict[rand].to_owned()
// }
