use rand::prelude::*;
pub fn gen_symbol(dictionary: &str) -> String {
    //Let's split up the dictionary by whitespace
    let dict: Vec<&str> = dictionary.split_whitespace().collect();
    //And we get a random word out of it
    let rand = thread_rng().gen_range(0, dict.len());
    dict[rand].to_owned()
}
